import React, { Component } from "react";
import { connect } from "react-redux";

class KetQua extends Component {
  render() {
    return (
      <div>
        <div className="display-4">
          BẠN CHỌN:{" "}
          <span className="text-danger">
            {this.props.taiXiu ? "TÀI" : "XỈU"}
          </span>
        </div>
        <div className="display-4 my-2">
          BÀN THẮNG:{" "}
          <span className="text-success">{this.props.soBanThang}</span>
        </div>
        <div className="display-4">
          TỔNG SỐ BÀN CHƠI:{" "}
          <span className="text-primary">{this.props.tongSoBanChoi}</span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    soBanThang: state.GameXucXacReducer.soBanThang,
    tongSoBanChoi: state.GameXucXacReducer.tongSoBanChoi,
    taiXiu: state.GameXucXacReducer.taiXiu,
  };
};

export default connect(mapStateToProps, null)(KetQua);
