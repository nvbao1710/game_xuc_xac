import React, { Component } from "react";
import { connect } from "react-redux";

class XucXac extends Component {
  renderXucXac = () => {
    return this.props.mangXucXac.map((item, index) => {
      return (
        <img
          key={index}
          style={{ width: 55, height: 55 }}
          src={item.hinhAnh}
          alt={item.hinhAnh}
        />
      );
    });
  };

  render() {
    return <div className="mt-5">{this.renderXucXac()}</div>;
  }
}

const mapStateToProps = (state) => {
  return {
    mangXucXac: state.GameXucXacReducer.mangXucXac,
  };
};

export default connect(mapStateToProps, null)(XucXac);
