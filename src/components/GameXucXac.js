import React, { Component } from "react";
import { connect } from "react-redux";
import KetQua from "./KetQua";
import "./styles.css";
import XucXac from "./XucXac";

class GameXucXac extends Component {
  render() {
    return (
      <div className="game ">
        <div className="title_game text-center mt-3 display-2">
          Game Xúc Xắc
        </div>
        <div className="row text-center">
          <div className="col-5 ">
            <button
              className="btnGame"
              onClick={() => {
                this.props.datCuoc(true);
              }}
            >
              Tài
            </button>
          </div>
          <div className="col-2 ">
            <XucXac />
          </div>
          <div className="col-5 ">
            <button
              className="btnGame"
              onClick={() => {
                this.props.datCuoc(false);
              }}
            >
              Xỉu
            </button>
          </div>
        </div>
        <div className="ketqua text-center">
          <KetQua />
          <button
            className="btnPlay mt-3"
            onClick={() => {
              this.props.playGame();
            }}
          >
            Play Game
          </button>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    datCuoc: (taiXiu) => {
      dispatch({
        type: "DAT_CUOC",
        taiXiu,
      });
    },
    playGame: () => {
      dispatch({ type: "PLAY_GAME" });
    },
  };
};

export default connect(null, mapDispatchToProps)(GameXucXac);
