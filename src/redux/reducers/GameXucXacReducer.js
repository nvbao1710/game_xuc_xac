const stateDafault = {
  taiXiu: true, //tru: tài (<=11) - false: xỉu (11<)
  mangXucXac: [
    { ma: 1, hinhAnh: "./img/gameXucXac/1.png" },
    { ma: 3, hinhAnh: "./img/gameXucXac/3.png" },
    { ma: 6, hinhAnh: "./img/gameXucXac/6.png" },
  ],
  soBanThang: 0,
  tongSoBanChoi: 0,
};

const GameXucXacReducer = (state = stateDafault, action) => {
  switch (action.type) {
    case "DAT_CUOC":
      state.taiXiu = action.taiXiu;
      return { ...state };
    case "PLAY_GAME": {
      let mangXucXacRanDom = [];
      for (let i = 0; i < 3; i++) {
        let soNgauNhien = Math.floor(Math.random() * 6) + 1;
        let xucXacNgauNhien = {
          ma: soNgauNhien,
          hinhAnh: `./img/gameXucXac/${soNgauNhien}.png`,
        };
        mangXucXacRanDom.push(xucXacNgauNhien);
      }
      state.mangXucXac = mangXucXacRanDom;
      state.tongSoBanChoi += 1;
      let tongSoDiem = mangXucXacRanDom.reduce((tongDiem, xucXac, index) => {
        return (tongDiem += xucXac.ma);
      }, 0);
      if (
        (tongSoDiem > 11 && state.taiXiu) ||
        (tongSoDiem <= 11 && !state.taiXiu)
      ) {
        state.soBanThang += 1;
      }

      return { ...state };
    }
    default:
      return { ...state };
  }
};
export default GameXucXacReducer;
